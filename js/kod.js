var plansza = [ ["","","","","",""],
				["","","","","",""],
				["","","","","",""],
				["","","","","",""],
				["","","","","",""],
				["","","","","",""]
];

var test = { name: 'world',
	plansza: plansza
 }


 /* Tak można ustawić kolor planszy */

 plansza[0][1] = 'czerwony';

 
 // Spakujemy to razem, i uruchomimy kiedy już plansza będzie narysowana
 function gra(){

 	var kolorNastepnegoKlocka = "zielony"

 	function zamienKolorKlocka(){
 		if (kolorNastepnegoKlocka == "zielony") kolorNastepnegoKlocka = "czerwony"
 			else kolorNastepnegoKlocka = "zielony";
 	}

 	// Po to istnieją funkcje
 	function rzuczKlocek(kolumna){
		for (var i = 0; i<6 ; i++){
 			if (i==5){
 				plansza[i][kolumna] = kolorNastepnegoKlocka; 				
 				zamienKolorKlocka();
 				break;
 			};
 			
 			if (plansza[i+1][kolumna]!= ""){
 				plansza[i][kolumna] = kolorNastepnegoKlocka;
 				zamienKolorKlocka();
 				// wazne - ustawialsmy wiecej niz trzeba
 				break;
 			}
 		} 
 		/* Czary mary, zawsze po zmianie planszy */
 		ractive.update();
 	}


 	$("#kol1").click( function (){ 
 		rzuczKlocek(0); 		
 	});

	$("#kol2").click( function (){ 
 		rzuczKlocek(1); 		
 	});

 	$("#kol3").click( function (){ 
 		rzuczKlocek(2); 		
 	});

 	$("#kol4").click( function (){ 
 		rzuczKlocek(3); 		
 	});

 	$("#kol5").click( function (){ 
 		rzuczKlocek(4); 		
 	});

 	$("#kol6").click( function (){ 
 		rzuczKlocek(5); 		
 	});
 }
